<?php

class PicModel extends CI_Model {
    public function __construct() 
    {
        parent::__construct();
        $config['hostname'] = "localhost";
        $config['username'] = "jharvard";
        $config['password'] = "crimson";
        $config['database'] = "jharvard_project1";
        $config['dbdriver'] = "mysql";
        $config['dbprefix'] = "";
        $config['pconnect'] = FALSE;
        $config['db_debug'] = TRUE;
        $config['cache_on'] = FALSE;
        $config['cachedir'] = "";
        $config['char_set'] = "utf8";
        $config['dbcollat'] = "utf8_general_ci";

        $this->load->database($config);
    }
    
    /* Returns an array of all the urls in the pics database */
    public function returnAllPics ()
    {
        $this->db->select('url')->from('pictures');
        return $this->db->get()->result_array();
    }
    
    /* Takes a pic id and returns an array of the url
       for that picture */
    public function getUrl ($id)
    {
        $this->db->select('url')->from('pictures')->where('id',$id);
        return $this->db->get()->result_array();
    }  
    
    /* Takes a pic id and returns an array of the description
       for that picture */
    public function getDesc ($id)
    {
        $this->db->select('description')->from('pictures')->where('id',$id);
        return $this->db->get()->result_array();
    }  
    
    /* Takes a pic id and returns an array of all the comments in the 
       comments database for that picture */
    public function getNotes ($id)
    {
        $this->db->select('notes.id,name,note,time')->from('notes');
        $this->db->join('pictures', 'pictures.id = notes.id', 'left');
        $this->db->where('notes.id',$id)->order_by("time", "desc");
        return $this->db->get()->result_array();
    }
    
    /* Takes a pic id and returns an array of how many hugs that 
       picture has */
    public function getHugs ($id)
    {
        $this->db->select('hugs')->from('pictures')->where('id',$id);
        return $this->db->get()->result_array();
    }
    
    /* Takes a pic id and increments the hug counter */
    public function hug ($id)
    {
        $this->db->set('hugs', 'hugs + 1', FALSE)->where('id',$id)->update('pictures');
    }
    
    /* Takes a string input and returns an array of pictures with that string
       in their description */
    public function search ($input)
    {  
        $this->db->like('description', $input);
        return $this->db->get('pictures')->result_array();
    }
    
    /* Takes a picture ID and name and comment and adds the comment to the comment 
       database for that particular ID  */
    public function addNote ($id, $name, $note)
    {  
        $data = array(
            'id' => $id ,
            'name' => $name ,
            'note' => $note
        );
        $this->db->insert('notes',$data);
    }
}
?>


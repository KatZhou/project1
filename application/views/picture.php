    <?php 
        echo '<div id="display">
            <img id="displayBorder" src='.$data['url'].'>
        </div>';
        echo '<div id = "hugs">
            <p>
                <strong>Hugs:</strong> '.$data['hugs'].' 
                <a data-role="button" data-inline="true" onClick="hug('.$data['id'].')" href="#">Hug</a>
            </p>
        </div>';
        echo '<p>
            '.$data['desc'].'
        </p>';
        echo "<p>
            <a data-role='button' data-icon='star' data-inline='true'
                onclick='add(JSON.stringify(".json_encode($data)."))'>
                Love me!</a>
        </p>";
        echo '<p>
            <strong>Leave a note:</strong>
            <form action="/picctrl/note/'.$data['id'].'" method="post" data-ajax="false" onSubmit="return notEmptyNote(this);">
                <label for="name">Name:</label>
                <input type="text" name="name" id="name" value=""  />
                <label for="textarea">Note (under 1000 character, please!):</label>
                <textarea name="note" id="textarea"></textarea>
                <input type="submit" value="Talk to me!" onclick="refresh()">
            </form>
        </p>';
        echo '<p>
            <h3>Notes:</h3>
        </p>';
        if (empty($data['notes']))
            echo '<p>
                Nobody loves me. :(
            </p>';
        else
        {
            foreach ($data['notes'] as $note)
            {
                echo '<p>
                    <strong>'.$note['name'].':</strong> '.$note['note'].'
                </p>';
            } 
        }
    ?>
    <a data-role="button" href="/">Back to ALL the pictures!</a>

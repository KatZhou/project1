	<h1>Search Results</h1>
	
	<div id="lists" class="ui-grid-b">
	<?php
	    $counter = 0;
	    $urls = $results['urls'];
	    $ids = $results['ids'];
	    for ($i = 0, $n = count($urls); $i < $n; $i++)
	    {
	        if ($counter == 0)
	        {
	            echo '<div class="ui-block-a">
	                <div class="thumb">
	                    <a href="/picctrl/picture/'.($ids[$i]).'"><img src='.$urls[$i].'></a>
	                </div>
	            </div>';
	            $counter = 1;
	        }
	        else if ($counter == 1)
	        {
	            echo '<div class="ui-block-b">
	                <div class="thumb">
	                    <a href="/picctrl/picture/'.($ids[$i]).'"><img src='.$urls[$i].'></a>
	                </div>
	            </div>';
	            $counter = 2;
	        }
	        else if ($counter == 2)
	        {
	            echo '<div class="ui-block-c">
	                <div class="thumb">
	                    <a href="/picctrl/picture/'.($ids[$i]).'"><img src='.$urls[$i].'></a>
	                </div>
	            </div>';
	            $counter = 0;
	        }
	    }
	?>
	</div>
	<a data-role="button" href="/">Back to ALL the pictures!</a>

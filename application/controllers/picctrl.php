<?php 

class PicCtrl extends CI_Controller {
    public function __construct() 
    {
        parent::__construct();
        $this->load->model('PicModel');
    }
    
    /* Load index page */
	public function index()
	{
		$picsArray = $this->PicModel->returnAllPics();
		$allPics = $this->pullElements($picsArray,'url');
		$this->load->view('header');
		$this->load->view('main', array ('allPics' => $allPics));
		$this->load->view('footer');
	}
	
	/* Load love list */
	public function loveList()
	{
		$this->load->view('header');
		$this->load->view('lovelist');
		$this->load->view('footer');
	}
	
	/* Increment hug counter for an image of a particular id */
	public function hug($id)
	{
		$this->PicModel->hug($id);
	}
	
	/* Load individual picture pages, passing them an array
	   containing the picture's url, the comments on that picture,
	   the description of that picture, and the number of hugs */
	public function picture($id)
	{
	    $this->load->view('header');
	    
	    $url = $this->pullElements($this->PicModel->getUrl($id),'url');
	    $desc = $this->pullElements($this->PicModel->getDesc($id),'description');
	    $notes = $this->PicModel->getNotes($id);    
	    $hugs = $this->pullelements($this->PicModel->getHugs($id),'hugs');
	    $data = array
	        ( 
	            'id' => $id,
	            'url'=> $url[0], 
	            'desc' => $desc[0],
                'notes'=> $notes, 
                'hugs'=> $hugs[0] 
            ); 
	    $this->load->view('picture', array('data' => $data));
	    
	    $this->load->view('footer');
	}
	
	/* Load search results page */
	public function results()
	{
	    $input = $this->input->post('search');
	    
	    $this->load->view('header');            
        $result = $this->PicModel->search($input);
        $urls = $this->pullElements($result,'url');
        $ids = $this->pullElements($result,'id');
        $results = array('urls' => $urls,'ids' => $ids);
        $this->load->view('search', array('results' => $results));
	    $this->load->view('footer');
	}
	
	/* Adds a comment for a picture of a specific ID */
	public function note($id)
	{
	    $name = $this->input->post('name');
	    $note = $this->input->post('note');       
        $this->PicModel->addNote($id, $name, $note);
        header('Location: /picctrl/picture/'.$id);
	}
    
    /* Takes an array of associative arrays (what mysql commands return)
       and one of the categories in the associative array
       and returns just an array with the information inside the associative
       arrays */
    private function pullElements($array,$cat)
    {
        $result = array();
        foreach ($array as $elt):
		    array_push($result, $elt[$cat]);
		endforeach;
		return $result;
    }
}
?>

-- MySQL dump 10.13  Distrib 5.5.20, for Linux (i686)
--
-- Host: localhost    Database: jharvard_project1
-- ------------------------------------------------------
-- Server version	5.5.20

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `notes`
--

DROP TABLE IF EXISTS `notes`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `notes` (
  `id` int(10) unsigned NOT NULL,
  `name` varchar(100) NOT NULL,
  `note` varchar(1000) NOT NULL,
  `time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `notes`
--

LOCK TABLES `notes` WRITE;
/*!40000 ALTER TABLE `notes` DISABLE KEYS */;
INSERT INTO `notes` VALUES (6,'Corinne','Smoosh','2012-03-21 22:12:43'),(6,'Corinne','SMOOSH','2012-03-21 22:12:59');
/*!40000 ALTER TABLE `notes` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `pictures`
--

DROP TABLE IF EXISTS `pictures`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `pictures` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `url` varchar(255) NOT NULL,
  `description` varchar(255) NOT NULL,
  `hugs` int(50) unsigned NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=31 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `pictures`
--

LOCK TABLES `pictures` WRITE;
/*!40000 ALTER TABLE `pictures` DISABLE KEYS */;
INSERT INTO `pictures` VALUES (1,'/images/malan.jpg','Obese Dolphin meets the legendary David Malan.',0),(2,'/images/programming.jpg','Obese Dolphin does his computer science homework.',0),(3,'/images/dinner.jpg','Obese Dolphin goes out for dinner.',0),(4,'/images/fallen.jpg','Obese Dolphin has fallen and can\'t get up.',0),(5,'/images/pokemon.jpg','Obese Dolphin is a Pokemon Trainer.',0),(6,'/images/smooshed.jpg','Obese Dolphin gets smooshed.',0),(7,'/images/reading.jpg','Obese Dolphin reads with a friend.',0),(8,'/images/fly.jpg','Obese Dolphin learns to fly.',0),(9,'/images/pudge.jpg','Obese Dolphin poses with his beautiful daughter, Pudge.',0),(10,'/images/cookie.jpg','Obese Dolphin noms on a fish-shaped cookie.',0),(11,'/images/mbta.jpg','Obese Dolphin takes the T.',0),(12,'/images/giraffe.jpg','Obese Dolphin bestows a kiss upon a giraffe-shaped mitten.',0),(13,'/images/seaLion.jpg','Obese Dolphin meets some sea lions.',0),(14,'/images/penguins.jpg','Obese Dolphin visits some penguins.',0),(15,'/images/fish.jpg','Obese Dolphin meets some fish that are friends, not food.',0),(16,'/images/shark.jpg','Obese Dolphin meets a shark but is not scared.',0),(17,'/images/fish2.jpg','Obese Dolphin meets a large fish named Bertha.',0),(18,'/images/seal.jpg','Obese Dolphin meets a seal.',0),(19,'/images/dolphins.jpg','Obese Dolphin meets some non-obese dolphins.',0),(20,'/images/dolphman.jpg','Obese Dolphin meets a dolphman (a rare species that is half dolphin, half human).',0),(21,'/images/roundShark.jpg','Obese Dolphin says hello to an obese shark.',0),(22,'/images/friends.jpg','Obese Dolphin has many friends.',0),(23,'/images/grope.jpg','Obese Dolphin gets groped by a stranger.',0),(24,'/images/poster.jpg','Obese Dolphin bows to his David Malan poster.',0),(25,'/images/sealFriend.jpg','Obese Dolphin gives a ride to his friend, Wentworth the fluffy seal.',0),(26,'/images/sleeping.jpg','Obese Dolphin takes a nap.',0),(27,'/images/sloth.jpg','Obese Dolphin meets and herpderps with a sloth named Slowpoke.',0),(28,'/images/bunny.jpg','Obese Dolphin pretends to be a bunny rabbit.',0),(29,'/images/bowtie.jpg','Obese Dolphin gets dressed up and wears a bowtie.',0),(30,'/images/spongebob.jpg','Obese Dolphin meets Spongebob Squarepants and gives him a ride.',0);
/*!40000 ALTER TABLE `pictures` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2012-03-23  1:21:03

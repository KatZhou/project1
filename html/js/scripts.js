function add (pic)
{
    for (var i = 0; i < 50; i++)
    {
        var curr = localStorage.getItem('pic'+i);
        if (pic == curr)
        {
            alert ("You already love this picture!");
            return;
        }
        if (curr == null || curr == "null" || curr == "undefined")
        {
            localStorage.setItem('pic'+i, pic);
            alert("Yay!!");
            return;
        }
    }
    alert ("You can't have moar than 50 favorites at once. :(");
}

function remove (num)
{
    for (var i = num; i < 50; i++)
    {
        var next = localStorage.getItem('pic'+(i+1));
        localStorage.setItem('pic'+i, next);
    }
    location.reload();
}

function notEmptySearch (input)
{
    if (input.search.value == "")
    {
        alert("You didn't enter anything in the search!");
        return false;
    }
    return true;
}

function notEmptyNote (input)
{
    if (input.name.value == "")
    {
        alert("You didn't enter your name!");
        return false;
    }
    else if (input.note.value == "")
    {
        alert("You didn't write me anything!");
        return false;
    }
    return true;
}

function hug (id)
{
    $.post("/picctrl/hug/"+id,function()
    {
        window.location.reload();
    });
}

function refresh ()
{
    window.location.reload();
}
